# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 20:24:36 2020

@author: SheldonWarden
"""

import numpy as np
import pandas as pd
from scipy import signal
#from numpy.polynomial.polynomial import polyfit 
import matplotlib.pyplot as plt
import pdb
import pywt
import datetime
import random
import gzip
import shutil
import os
from scipy import stats
from myconfig import *
import shutil
import matplotlib.dates as mdates
import time

def nan_helper(y):
        "Taken from https://stackoverflow.com/questions/6518811/interpolate-nan-values-in-a-numpy-array"
        return np.isnan(y), lambda z: z.nonzero()[0]

def printProgressBar(iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    "Taken from https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console"
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
#    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = printEnd)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix))
    # Print New Line on Complete
    if iteration == total: 
        print()

def create_output_folders_and_subfolders(root_folder,output_folder):
    if not os.path.isdir(str(root_folder)+str(output_folder)):    
        os.mkdir(str(root_folder)+str(output_folder))
        os.mkdir(str(root_folder)+str(output_folder)+'/kak')
        os.mkdir(str(root_folder)+str(output_folder)+'/kny')
        os.mkdir(str(root_folder)+str(output_folder)+'/kak_vs_kny')
    else:
        print("Output folders and subfolders already exist!")
        
def back_up_the_configuration_file(root_folder,output_folder):
    shutil.copy('myconfig.py', str(root_folder)+str(output_folder))
    

def extract_gunzipped_file(gzfilename,secfilename):
    with gzip.open(gzfilename, 'rb') as f_in:
        with open(secfilename, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
    return 

def identify_region(min_radius):
    if min_radius==0:
        print('Considering region A (since inner radius is 0)')
    else:
        print('Considering region B')
        
def randomly_select_NOQ_days(number_of_quakes,start_date,end_date):
    list_of_dates = [start_date + datetime.timedelta(days=x) for x in range(0, (end_date-start_date).days)]
    list_of_randomly_selected_days=[]
    for i in np.arange(number_of_quakes):
        list_of_randomly_selected_days.append(random.choice(list_of_dates))
    return list_of_randomly_selected_days

def compute_p_count_for_randomly_selected_days(list_of_randomly_selected_days,p_array,p_threshold,start_date,end_date):
    p_count_1_day_stack_random=np.zeros(91)
    for idx,random_date in enumerate(list_of_randomly_selected_days):
        tdelta=random_date-start_date
        center_idx=tdelta.days
        start_idx=center_idx-45
        end_idx=center_idx+46
        p_91=p_array[start_idx:end_idx]
        p_count=np.zeros(91)
        p_count[np.where(p_91>p_threshold)]=1
        p_count_1_day_stack_random=p_count_1_day_stack_random+p_count
    return p_count_1_day_stack_random
        
def compute_random_mean(niter,number_of_quakes,start_date,end_date,p_array,p_threshold):
    p_count_1_day_stack_over_NITER=np.zeros([niter,91])    
    for i in np.arange(niter):
        list_of_randomly_selected_days=randomly_select_NOQ_days(number_of_quakes,start_date,end_date)
        p_count_1_day_stack_random=compute_p_count_for_randomly_selected_days(list_of_randomly_selected_days,p_array,p_threshold,start_date,end_date)
        p_count_1_day_stack_over_NITER[i]=p_count_1_day_stack_random 
        random_mean=np.mean(p_count_1_day_stack_over_NITER,0) 
        random_std=np.std(p_count_1_day_stack_over_NITER,0) 
    return p_count_1_day_stack_over_NITER,random_mean,random_std            
    
def compute_5_days_stack_over_NITER(p_count_1_day_stack_over_NITER,niter):
    p_count_5_days_stack_over_NITER=np.zeros([niter,18])
    for i in np.arange(0,9):
        p_count_5_days_stack_over_NITER[:,i]=np.sum(p_count_1_day_stack_over_NITER[:,0+5*i:5+5*i],1)
    for i in np.arange(9,18):
        p_count_5_days_stack_over_NITER[:,i]=np.sum(p_count_1_day_stack_over_NITER[:,46+5*(i-9):51+5*(i-9)],1)
    random_mean_5_days=np.mean(p_count_5_days_stack_over_NITER,0) 
    random_std_5_days=np.std(p_count_5_days_stack_over_NITER,0)
    return p_count_5_days_stack_over_NITER,random_mean_5_days,random_std_5_days


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

class Quakes(object):

    def __init__(self):
        self.data = None
        self.number_of_quakes = None
        
    def populate(self):
        self.data=pd.read_csv('./earthquake_catalog_20191124.csv', header=0, index_col=False, delim_whitespace=False) ## Read data from earthquake catalog.

#        self.raw_data=pd.read_csv('E:/USGS_data_analysis/quakes_450-453_local_dates_updated.csv', header=0, index_col=False, delim_whitespace=False)
#        self.raw_data=pd.read_table('E:/USGS_data_analysis/quakes_450-453.20190708.qfdbdev01.txt', header=0, index_col=False, delim_whitespace=False)
        self.data.to_csv(str(root_folder)+'catalog_processing/raw_data.csv')
                      
    def manuallyselectquakes(self, date_list):    
        print("Selecting earthquake days from date list input in the configuration file" )
        col_names =  ['time']
        self.data  = pd.DataFrame(data=date_list,columns = col_names)
#        self.number_of_quakes=self.filtered_data.shape[0]
        self.data.to_csv(str(root_folder)+'catalog_processing/raw_data.csv')

    def automaticallyselectquakes(self):
        print("Automatically selecting earthquake days" )
        self.populate()    
        print("Working with "+str(catalog)+" source catalog ")
        self.data=self.data[(self.data['source_catalog']==catalog)] 
        print("Considering local station "+str(station_number))
        self.data=self.data[(self.data['station_id']==station_number)]
        self.data=self.data[(pd.to_datetime(self.data.time) > start_date+datetime.timedelta(45)) & (pd.to_datetime(self.data.time) < end_date-datetime.timedelta(45))]          
        print("Considering data between "+(start_date+datetime.timedelta(45)).strftime("%d/%m/%Y")+" and "+(end_date-datetime.timedelta(45)).strftime("%d/%m/%Y"))
        self.data=self.data[(self.data['distance']>min_radius) & (self.data['distance']<max_radius)]    
        self.data=self.data[(self.data['depth']>min_depth)] 
        self.data=self.data[(self.data['depth']<max_depth)] 
        print("Considering hypocentral depths comprised between "+str(min_depth)+" and "+str(max_depth)+"km")
        self.data['number_of_earthquakes_per_day']=1 
#        dq.data.to_csv(str(root_folder)+'TEMP_BEFOREGROUPBY.csv') ## csv file created for debugging purpose only
        dq_time=self.data.groupby(['date']).max()             
        dq_energy=self.data.groupby(['date']).sum() 
        self.data=pd.concat([dq_time[['time','magnitude']], dq_energy[['energy','number_of_earthquakes_per_day']]], axis=1, sort=False)
        self.data['Es_new']=np.log10(self.data['energy'])
        self.data=self.data[(self.data['Es_new']>np.log10(integrated_es))]
        self.data.to_csv(str(root_folder)+'VALIDATION.csv')
        
        
    def select_qualifying_earthquake_days(self, manually_select_quakes):
#        dq=Quakes() #Instantiate an object of type Quakes
        if manually_select_quakes==1: ## Fetch the earthquake date list from the config file
            self.manuallyselectquakes(date_list)
        else: ## Use the earthquake date selection strategy detailed in Han et al., 2014
            self.automaticallyselectquakes()      
    #    dq.data.drop(['2013-03-26'], inplace=True)     
        self.number_of_quakes=self.data.shape[0]      
        print("Considering "+str(self.number_of_quakes)+" earthquake days in the study interval")
        dq=self
        A=self.data
        return dq, A

          
                
class RawData(object):

    def __init__(self):
        self.date = None
        self.year = None
        self.station_id= None
        self.data = None
        self.decimated_data = None
        self.number_of_missing_samples = 0
        self.data_type = None

    def populate(self,station_id,date,data_type):
        self.data_type = data_type
        self.date = date
        self.year = date.year
        self.station_id = station_id
#        print("Loading: "+str(date))
#        gzfilename='E:/usgs_japan/usgs/'+date.strftime('%Y')+'UTC/definitive/second/'+station_id+date.strftime('%Y')+date.strftime('%m')+date.strftime('%d')+'dsec.sec.gz'
#        gzfilename='E:/usgs_japan_post2010/usgs/'+date.strftime('%Y')+'UTC/definitive/second/'+station_id+date.strftime('%Y')+date.strftime('%m')+date.strftime('%d')+'dsec.sec.gz'
        gzfilename=str(root_folder)+str(input_folder)+'/usgs/'+date.strftime('%Y')+'UTC/definitive/second/'+station_id+date.strftime('%Y')+date.strftime('%m')+date.strftime('%d')+'dsec.sec.gz'

#        if os.path.exists(gzfilename):
        secfilename='.'.join(gzfilename.split('.')[:-1])
        if os.path.exists(secfilename):
#            extract_gunzipped_file(gzfilename,secfilename)
            count = len(open(secfilename).readlines(  ))
#            print(count)
            if count > 86419:
                df=pd.read_table(secfilename, header=19, delim_whitespace=True)
                df.eval(station_id.upper()+data_type)[df.eval(station_id.upper()+data_type) == 99999.00]=np.nan
                df.eval(station_id.upper()+data_type)[df.eval(station_id.upper()+data_type) == 88888.00]=np.nan
                self.data=np.asarray(df.eval(station_id.upper()+data_type))

                self.number_of_missing_samples=np.sum(np.isnan(self.data))
            else:
                print('SEC FILE DOES NOT EXIST')
                self.data=np.empty(86400)*np.nan
                self.number_of_missing_samples=86400  
#        else:
#            print('GUNZIPPED FILE DOES NOT EXIST')
#            self.data=np.empty(86400)*np.nan
#            self.number_of_missing_samples=86400
        self.decimated_data=signal.decimate(self.data,5,ftype='iir', axis=-1, zero_phase=True) 
            
    def plot_specgram_of_decimated_data(self, Fs, title='', x_label='', y_label='', fig_size=None): 
        plt.ioff()
        fig = plt.figure() 
        ax = fig.add_subplot(111) 
        pxx,  freq, t, cax = plt.specgram(self.decimated_data, Fs=Fs, detrend='mean', cmap='jet', scale='dB')
        ax.set_title(title,fontsize=40) 
        ax.set_xlabel(x_label,fontsize=40) 
        ax.set_ylabel(y_label,fontsize=40)
        ax.set_ylim([0,0.1])
        ax.set_xlim([0,t[-1]])
        ax.set_xticks(np.arange(7)*t[-1]/6) 
        ax.set_xticklabels(('00:00','04:00','08:00','12:00','16:00','20:00','24:00')) 
        ax.tick_params(labelsize=40)
        ax.axvline(int(16*3600+90*60),color='black',linestyle='--')
        ax.axvline(int(16*3600+180*60),color='black',linestyle='--')

        fig.colorbar(cax).ax.tick_params(labelsize=40)
#        fig.colorbar(cax).set_label('Intensity (dB)')
#        fig.colorbar(cax).ax.set_label('Intensity [dB]')
        fig.set_size_inches(18.5, 10.5) 
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.station_id)):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.station_id))
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.station_id)+'/SPEGRAM_'+self.station_id+'_'+self.data_type+'_'+self.date.isoformat()[:10]+'.png') 
        plt.close(fig)

class QuietTimeInterval(object):
    
    def __init__(self):
        self.data = None
        self.station_id= None
        self.data_type = None
        self.DWTcoeffs = None
        self.detailed_data= None
        self.energy = np.nan
        self.number_of_missing_samples = 0

    def populate(self,station_id,date,data_type):
        self.data_type=data_type
        self.date=date
        self.station_id= station_id
        start_time=datetime.time(17,10,0)
        end_time=datetime.time(19,20,0)
#        start_time=datetime.time(17,12,0)
#        end_time=datetime.time(19,18,0)

        start_sample=start_time.hour*3600+start_time.minute*60+start_time.second
        end_sample=end_time.hour*3600+end_time.minute*60+end_time.second
        rd=RawData() 
        rd.populate(station_id,date,data_type)
#        rd.plot_specgram_of_decimated_data(Fs=0.2,title=str(self.data_type)+'_'+str(self.date.date()), x_label='UTC (hh:mm)', y_label='Frequency (Hz)')
        self.data = rd.data[start_sample:end_sample]
        self.number_of_missing_samples = np.sum(np.isnan(self.data))
        
    def apply_dwt(self,wavelet_type):
        if self.data is not None:
            coeffs = pywt.wavedec(self.data,wavelet_type,mode='symmetric', level=6, axis=-1)
            self.DWTcoeffs=coeffs

    def extract_detailed_signals_in_the_fourth_level(self,wavelet_type):
        if self.DWTcoeffs is not None:
            self.DWTcoeffs[0] = np.zeros_like(self.DWTcoeffs[0])
            self.DWTcoeffs[-1] = np.zeros_like(self.DWTcoeffs[-1])
            self.DWTcoeffs[-2] = np.zeros_like(self.DWTcoeffs[-2])
            self.DWTcoeffs[-3] = np.zeros_like(self.DWTcoeffs[-3])
            self.DWTcoeffs[-5] = np.zeros_like(self.DWTcoeffs[-5])
            self.DWTcoeffs[-6] = np.zeros_like(self.DWTcoeffs[-6])
            reconstructed_chunk=pywt.waverec(self.DWTcoeffs,wavelet_type,mode='symmetric',axis=-1) 
            self.detailed_data=reconstructed_chunk[1200:7800-1200]

    def extract_detailed_signals_in_the_fifth_level(self,wavelet_type):
        if self.DWTcoeffs is not None:
            self.DWTcoeffs[0] = np.zeros_like(self.DWTcoeffs[0])
            self.DWTcoeffs[-1] = np.zeros_like(self.DWTcoeffs[-1])
            self.DWTcoeffs[-2] = np.zeros_like(self.DWTcoeffs[-2])
            self.DWTcoeffs[-3] = np.zeros_like(self.DWTcoeffs[-3])
            self.DWTcoeffs[-4] = np.zeros_like(self.DWTcoeffs[-4])
            self.DWTcoeffs[-6] = np.zeros_like(self.DWTcoeffs[-6])
            reconstructed_chunk=pywt.waverec(self.DWTcoeffs,wavelet_type,mode='symmetric',axis=-1) 
            self.detailed_data=reconstructed_chunk[1200:7800-1200]
    
    def extract_detailed_signals_in_the_sixth_level(self,wavelet_type):
        if self.DWTcoeffs is not None:
            self.DWTcoeffs[0] = np.zeros_like(self.DWTcoeffs[0])
            self.DWTcoeffs[-1] = np.zeros_like(self.DWTcoeffs[-1])
            self.DWTcoeffs[-2] = np.zeros_like(self.DWTcoeffs[-2])
            self.DWTcoeffs[-3] = np.zeros_like(self.DWTcoeffs[-3])
            self.DWTcoeffs[-4] = np.zeros_like(self.DWTcoeffs[-4])
            self.DWTcoeffs[-5] = np.zeros_like(self.DWTcoeffs[-5])
            reconstructed_chunk=pywt.waverec(self.DWTcoeffs,wavelet_type,mode='symmetric',axis=-1) 
            self.detailed_data=reconstructed_chunk[1200:7800-1200]
#            self.detailed_data=reconstructed_chunk[1080:7560-1080]

    
    def extract_detailed_signals(self, wavelet_type, level=6):
        if level==6:
            detailed_signals=self.extract_detailed_signals_in_the_sixth_level(wavelet_type)
        elif level==5:
            detailed_signals=self.extract_detailed_signals_in_the_fifth_level(wavelet_type)            
        elif level==4:
            detailed_signals=self.extract_detailed_signals_in_the_fourth_level(wavelet_type)  
        else:
            print('Level not yet implemented! Returning sixth level by default!')
            self.extract_detailed_signals_in_the_sixth_level(self, wavelet_type)
        return detailed_signals
        
    def plot_detailed_signals_in_the_sixth_level(self):
        plt.suptitle("6th level decomposition for station "+str(self.station_id))
        plt.plot(self.detailed_data, color='cornflowerblue',linewidth=2)
        plt.xlabel("LT",fontsize=20)
        plt.ylabel("Amplitude (nT)",fontsize=20)
        plt.xticks(np.arange(4)*len(self.detailed_data)/3)
        plt.tick_params(axis='both',labelsize=20)
        ax=plt.gca()
        ax.set_xticklabels(('02:30','03:00','03:30','04:00'))
        fig = plt.gcf()
        fig.set_size_inches(40,10)
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.station_id)+'/REGEN_'+str(self.station_id)+'_'+str(self.data_type)+'_'+self.date.isoformat()[:10]+'.png')
        plt.close(fig)

    def compute_daily_energy(self):
        if self.detailed_data is not None:
#            daily_energy=np.sum(np.power(self.detailed_data, 2))
            daily_energy=np.var(self.detailed_data)
            self.energy=daily_energy
#            self.energy=daily_energy/5400
            
        
class QuietTimeIntervalDoublet(object):
    
    def __init__(self):
        self.detailed_data_local = None
        self.detailed_data_remote = None
        self.station_id_local = None
        self.station_id_remote = None
        self.data_type = None   
        self.date = None
        
    def populate(self,station_id_local,station_id_remote,date,data_type,wavelet_type):
        qtil=QuietTimeInterval()
        qtil.populate(station_id_local,date,data_type)
        self.station_id_local=qtil.station_id
        self.date=qtil.date
        self.data_type=qtil.data_type
        qtil.apply_dwt(wavelet_type)
#        qtil.extract_detailed_signals_in_the_sixth_level(wavelet_type)        
        qtil.extract_detailed_signals(wavelet_type, 6)        

        self.detailed_data_local=qtil.detailed_data 

        qtir=QuietTimeInterval()
        qtir.populate(station_id_remote,date,data_type)
        self.station_id_remote=qtir.station_id
        qtir.apply_dwt(wavelet_type)
#        qtir.extract_detailed_signals_in_the_sixth_level(wavelet_type)
        qtir.extract_detailed_signals(wavelet_type, 6)

        self.detailed_data_remote=qtir.detailed_data 
        
    def plot_detailed_signals_in_the_sixth_level(self):  
        plt.suptitle(str(self.data_type),fontsize=50)
#        plt.suptitle("6th level decomposition for stations "+str(self.station_id_local)+" and "+str(self.station_id_remote),fontsize=24)
#        plt.suptitle("Original data for stations "+str(self.station_id_local)+" and "+str(self.station_id_remote),fontsize=24)
        plt.plot(signal.detrend(self.detailed_data_local, axis=-1, type='constant', bp=0), color='red',linewidth=4,label=str(self.station_id_local))
        plt.plot(signal.detrend(self.detailed_data_remote, axis=-1, type='constant', bp=0), color='blue',linewidth=4,label=str(self.station_id_remote))
        plt.xlabel("LT",fontsize=50)
        plt.ylabel("Amplitude (nT)",fontsize=50)
        plt.xticks(np.arange(4)*len(self.detailed_data_local)/3)
        plt.tick_params(labelsize=50)
        plt.legend(fontsize=50)
        ax=plt.gca()
        ax.set_xticklabels(('02:30','03:00','03:30','04:00'),fontsize=50)        
        fig = plt.gcf()
        fig.set_size_inches(40,12)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.station_id_local)+'_vs_'+str(self.station_id_remote)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.station_id_local)+'_vs_'+str(self.station_id_remote)+'/')
#        plt.savefig('E:/'+str(output_folder)+'/'+str(self.station_id_local)+'_vs_'+str(self.station_id_remote)+'/RECONSTRUCTED_'+str(self.data_type)+'_'+self.date.isoformat()[:10]+'.png')
        plt.savefig(str(root_folder)+str(output_folder)+'/regenerated'+str(self.data_type)+'.png')

        plt.close(fig)

    def plot_psd(self):  
        plt.suptitle(str(self.data_type),fontsize=50)
#        plt.psd(self.data_local, color='black',linewidth=4, label='original data')

        plt.psd(self.detailed_data_local, scale_by_freq=True, color='red',linewidth=4,label='data reconsutructed using the 6th level')
#        plt.plot(signal.detrend(self.detailed_data_remote, axis=-1, type='constant', bp=0), color='blue',linewidth=4,label=str(self.station_id_remote))
        plt.xlim(left=0.0001, right=1)
        plt.ylim(bottom=-221,top=119)
        plt.xlabel("Frequency (Hz)",fontsize=50)
        plt.ylabel("PSD (dB/Hz)",fontsize=50)
        plt.tick_params(labelsize=50)
        plt.legend(fontsize=50)
        ax=plt.gca()
        ax.set_xscale('log')
        fig = plt.gcf()
        fig.set_size_inches(40,12)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.station_id_local)+'_vs_'+str(self.station_id_remote)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.station_id_local)+'_vs_'+str(self.station_id_remote)+'/')
#        plt.savefig('E:/'+str(output_folder)+'/'+str(self.station_id_local)+'_vs_'+str(self.station_id_remote)+'/RECONSTRUCTED_'+str(self.data_type)+'_'+self.date.isoformat()[:10]+'.png')
        plt.savefig(str(root_folder)+str(output_folder)+'/PSD'+str(self.data_type)+'.png')

        plt.close(fig)

      
class EnergyArray(object):
    
    def __init__(self):
        self.data = None
        self.number_of_missing_samples = None
        self.length = 0
        self.time_samples = None
        self.station_id = None
        self.data_type = None
        self.start_date = None
        self.end_date = None

    def populate(self, station_id,start_date,end_date,data_type,list_of_days_to_reject,wavelet_type='db5'):
        delta=end_date-start_date
        self.start_date = start_date
        self.end_date = end_date
        self.station_id = station_id
        number_of_days=delta.days
        if os.path.exists(str(root_folder)+str(output_folder)+'/'+self.station_id+'/'+self.station_id.upper()+'_energy_array.npy'):
            energy_array=np.load(str(root_folder)+str(output_folder)+'/'+self.station_id+'/'+self.station_id.upper()+'_energy_array.npy')
            missing_samples_array=np.zeros(number_of_days)
        else:
            energy_array=np.zeros(number_of_days)
            missing_samples_array=np.zeros(number_of_days)
            list_of_rejected_days=[]
            for idx in np.arange(number_of_days):
                date=start_date+datetime.timedelta(int(idx))
                qti=QuietTimeInterval()
                qti.populate(station_id,date,data_type)
                qti.apply_dwt(wavelet_type)
#                qti.extract_detailed_signals_in_the_sixth_level(wavelet_type)
                qti.extract_detailed_signals(wavelet_type,6)
 
    #            qti.plot_detailed_signals_in_the_sixth_level()
                qti.compute_daily_energy()
                if date in list_of_days_to_reject:
#                    print(date)
#                    print('REJECT DAY!!!!!')
                    qti.energy=np.nan
                if qti.energy>0.01:
                    print(date)
                    print("ENERGY IS" +str(qti.energy))
                energy_array[idx]=qti.energy
                missing_samples_array[idx]=qti.number_of_missing_samples
                if idx % 100 == 0:
                    printProgressBar(idx + 1, number_of_days, prefix = 'Loading '+str(date)+'; Progress:', suffix = 'Complete', length = 50)                
        #interpolate NaN values:
        nans, nanidx= nan_helper(energy_array)
        energy_array[nans]= np.interp(nanidx(nans), nanidx(~nans), energy_array[~nans])
        t=np.arange(start_date,end_date,datetime.timedelta(days=1))
        self.data = energy_array
        self.number_of_missing_samples=missing_samples_array
        self.length = number_of_days
        self.time_samples = t

        self.data_type = data_type

        
    def plot_energy_array(self):
        plt.plot(self.time_samples,self.data,color='red',linewidth=2, label=str(self.station_id))
        plt.xlabel('Time', fontsize=25)
#        if self.data_type=='d_mag_volts_rs1':
#            plt.ylabel('Energy of Z component', fontsize=10)
#        elif self.data_type=='n_mag_volts_rs1':
#            plt.ylabel('Energy of N component', fontsize=10)
        plt.ylabel('Energy', fontsize=25)
#        plt.title('Daily energy at station '+str(self.station_id), fontsize=20)
        plt.legend(fontsize=25)
        plt.tick_params(labelsize=25)
        fig=plt.gcf()
        fig.set_size_inches(15,8)
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.station_id)+'/'+str(self.station_id)+'_'+str(self.data_type)+'_energy_array.png')

        plt.close(fig)
        
    def plot_noms_array(self):
        plt.plot(self.time_samples,self.number_of_missing_samples,color='grey',linewidth=2)
        plt.xlabel('Time')
        if self.data_type=='d_mag_volts_rs1':
            plt.ylabel('NOMS Z component')
        elif self.data_type=='n_mag_volts_rs1':
            plt.ylabel('NOMS N component')
        plt.title('Daily energy at station '+str(self.station_id))
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.station_id)+'_noms_array.png')
        fig=plt.gcf()
        plt.close(fig)
     


class EnergyArrayDoublet(object):
    
    def __init__(self):
        self.local = None
        self.remote = None
        self.quakes = None
        self.t_array = None
        self.p_array = None
        self.predicted_energy_array = None
        self.threshold = None
        self.p_count_1_day_stack = None
        self.p_count_5_day_stack = None
        self.number_of_quakes = 0
        self.list_of_randomly_selected_days = None
        self.random_mean = None
        self.random_std = None
        self.number_of_events = 0
        self.quake_array = None
        self.molchan_array = None
        self.number_of_false_negatives = 0
        self.number_of_true_positives = 0
        self.x_false_negatives = None
        

    def populate(self, energy_array_local,energy_array_remote,quakes):
        self.local=energy_array_local
        self.remote=energy_array_remote
        pdb.set_trace()
#        self.quakes=quakes.data
        self.quakes=dq.data
        
    def produce_correlation_plot(self):      
        NOD=np.int(self.local.data.shape[0])
        denominator=NOD*np.sum(np.power(self.remote.data,2))-np.power(np.sum(self.remote.data),2)
        beta=(NOD*np.sum(self.remote.data*self.local.data)-np.sum(self.remote.data)*np.sum(self.local.data))/denominator
        c=(np.sum(np.power(self.remote.data,2))*np.sum(self.local.data)-np.sum(self.remote.data)*np.sum(self.local.data*self.remote.data))/denominator
        print("beta is "+str(beta))
        print("c is "+str(c))
        xvalues=np.linspace(start = 0, stop = 0.01, num = 100) 
        plt.plot(xvalues, 1/beta*xvalues-c/beta, color='red')
#        b, m = polyfit(self.local.data, self.remote.data, 1)
        plt.scatter(self.local.data,self.remote.data, facecolors='none', edgecolors='b', s=20, linewidth=10)
#        plt.plot(self.local.data, b + m * self.local.data, 'r-')     
        axes=plt.gca()
        axes.set_xlim(left=0,right=0.01)
        axes.set_ylim(bottom=0,top=0.01)
        pear=stats.pearsonr(self.local.data, self.remote.data)
        plt.suptitle("Pearson correlation coefficient: {:.2f}".format(pear[0]), fontsize=30)
#        plt.title(self.local.data_type)
        plt.xlabel("Energy of "+str(self.local.station_id).upper(), fontsize=30)
        plt.ylabel("Energy of "+str(self.remote.station_id).upper(), fontsize=30)
        plt.tick_params(labelsize=30)
        fig=plt.gcf()
        fig.set_size_inches(15,15)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/FIGURE_6_'+self.local.data_type+'_correlation_plot_betw_'+str(self.local.station_id)+'_and_'+str(self.remote.station_id)+'.png')

        plt.close(fig)
    
    def compute_t_array(self):
        t=np.arange(self.local.start_date,self.local.end_date,datetime.timedelta(days=1))
        self.t_array=t

    def compute_p_array(self):
#        b, m = polyfit(self.remote.data,self.local.data,1)
#        predicted_energy_array=b*np.ones(len(self.remote.data)) + m * self.remote.data
#        minval=np.min(self.local.data)
#        predicted_energy_array[predicted_energy_array<minval]=minval
        
        NOD=np.int(self.local.data.shape[0])
        denominator=NOD*np.sum(np.power(self.remote.data,2))-np.power(np.sum(self.remote.data),2)
        beta=(NOD*np.sum(self.remote.data*self.local.data)-np.sum(self.remote.data)*np.sum(self.local.data))/denominator
        c=(np.sum(np.power(self.remote.data,2))*np.sum(self.local.data)-np.sum(self.remote.data)*np.sum(self.local.data*self.remote.data))/denominator
        print("beta is "+str(beta))
        print("c is "+str(c))
       
        predicted_energy_array=beta*self.remote.data+c*np.ones(len(self.remote.data))        
        
        
        
        p_array=self.local.data/predicted_energy_array
        self.predicted_energy_array=predicted_energy_array
#        threshold_beyond_which_to_reject_aberrant_p_values=np.mean(p_array)+8*np.std(p_array)
#        p_array[p_array>threshold_beyond_which_to_reject_aberrant_p_values]=0        
        self.p_array=p_array        

    def reject_anomalous_p_values(self, threshold):
        self.t_array[self.p_array>threshold]=0
        self.p_array[self.p_array>threshold]=0  
        
    def compute_number_of_quakes(self):
        number_of_quakes=0
        for idx,eq_date in enumerate(self.quakes.time):
#            if (datetime.datetime.combine(eq_date.date(), datetime.datetime.min.time())-self.local.start_date) <= datetime.timedelta(45) or (self.local.end_date-datetime.datetime.combine(eq_date.date(), datetime.datetime.min.time()) <= datetime.timedelta(45)):
            if (datetime.datetime.strptime(eq_date, '%Y-%m-%d %H:%M:%S.%f')-self.local.start_date) <= datetime.timedelta(45) or (self.local.end_date-datetime.datetime.strptime(eq_date, '%Y-%m-%d %H:%M:%S.%f') <= datetime.timedelta(45)):

                print('Warning: not enough data for a 91 days window!!!')
            else:
                number_of_quakes=number_of_quakes+1
        self.number_of_quakes=number_of_quakes
        
    def plot_p_values_with_quake_dates(self):
        print('PLOT P VALUES WITH QUAKE DATES')
        plt.plot(self.t_array,self.p_array, color='olive',linewidth=6)
        for idx,eq_date in enumerate(self.quakes.time):
            plt.axvline(eq_date,linestyle='--',color='black')
#            plt.text(eq_date,0.5,eq_date[:10],rotation=90,fontsize=40)
        plt.axhline(self.threshold,color='red')
        plt.ylabel("Time",fontsize=40)
        plt.ylabel("P value",fontsize=40)
        plt.ylim([0,np.max(self.p_array)+0.5])
#        plt.ylim(0.998,1.002)
        plt.tick_params(labelsize=40)
        plt.suptitle("Station "+str(self.local.station_id),fontsize=40)
        plt.title(str(self.number_of_quakes)+" EQ",fontsize=40)
        ax=plt.gca()
        ax.yaxis.offsetText.set_fontsize(40)
        fig = plt.gcf()
        fig.set_size_inches(40,25)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'+str(self.local.station_id)+'_p_array_with_EQ_dates.png')
        plt.close(fig)
        return
        
    def plot_Z_and_p_arrays(self):       
        print('PLOT P AND Z ARRAYS')
        fig = plt.subplot(212)
        ax1 = plt.subplot(211)
        ax1.margins(0.05)           
        ax1.plot(self.t_array,self.local.data, color='lightgreen',linewidth=4,label='Z_'+str(self.local.station_id))
        ax1.plot(self.t_array,self.predicted_energy_array, color='red',linewidth=4,label='$Z^{*}$'+str(self.local.station_id))
        ax1.set_xlim(left=self.t_array.min(), right=self.t_array.max())
        ax1.set_ylim(bottom=0, top=0.002)
        ax1.set_ylabel("Energy of Z comp.",fontsize=50)
        ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
        ax1.legend(fontsize=50)
        ax1.tick_params(labelsize=50)
        ax1.yaxis.offsetText.set_fontsize(50)
        ax2 = plt.subplot(212)
        ax2.margins(0.05)           
        ax2.plot(self.t_array,self.p_array, color='cornflowerblue',linewidth=6, marker='o', markersize=18)
        ax2.set_ylabel("P value",fontsize=50)
        ax2.set_xlim(left=self.t_array.min(), right=self.t_array.max())
        ax2.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
        ax2.set_ylim(bottom=0, top=4)
        ax2.tick_params(labelsize=50)
        fig = plt.gcf()
        fig.set_size_inches(40,30)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/p_array_'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'.png')
        plt.close(fig)
        
    def plot_energy_and_P_values_over_30_days_FIGURE_7(self):        
        print('PLOT P AND Z ARRAYS')
        fig = plt.subplot(212)
        ax1 = plt.subplot(211)
        ax1.margins(0.05)           
        ax1.plot(self.t_array,self.local.data, color='lightgreen',linewidth=4,label='Z_'+str(self.local.station_id), marker='o', markersize=18)
        ax1.plot(self.t_array,self.predicted_energy_array, color='red',linewidth=4,label='$Z^{*}$'+str(self.local.station_id), marker='o', markersize=18)
        ax1.set_xlim(left=self.t_array.min(), right=self.t_array.max())
        ax1.set_ylim(bottom=0, top=0.002)
        ax1.set_ylabel("Energy of Z comp.",fontsize=50)
        ax1.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
        ax1.legend(fontsize=50)
        ax1.tick_params(labelsize=50)
        ax1.yaxis.offsetText.set_fontsize(50)
        ax2 = plt.subplot(212)
        ax2.margins(0.05)           
        ax2.plot(self.t_array,self.p_array, color='cornflowerblue',linewidth=6, marker='o', markersize=18)
        ax2.set_ylabel("P value",fontsize=50)
        ax2.set_xlim(left=self.t_array.min(), right=self.t_array.max())
        ax2.xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
        ax2.set_ylim(bottom=0, top=4)
        ax2.tick_params(labelsize=50)
        fig = plt.gcf()
        fig.set_size_inches(40,30)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/FIGURE_7_p_array_'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'.png')
        plt.close(fig)

    def plot_energy_and_P_values_over_10_years_FIGURE_8(self):        
        print('PLOT P AND Z ARRAYS')
        fig = plt.subplot(212)
        ax1 = plt.subplot(211)
        ax1.margins(0.05)           
        ax1.plot(self.t_array,self.local.data, color='lightgreen',linewidth=4,label='Z_'+str(self.local.station_id))
        ax1.plot(self.t_array,self.predicted_energy_array, color='red',linewidth=4,label='$Z^{*}$'+str(self.local.station_id))
        ax1.set_xlim(left=self.t_array.min(), right=self.t_array.max())
        ax1.set_ylim(bottom=0, top=0.01)
        ax1.set_ylabel("Energy of Z comp.",fontsize=50)
        ax1.legend(fontsize=50)
        ax1.tick_params(labelsize=50)
        ax1.yaxis.offsetText.set_fontsize(50)
        ax2 = plt.subplot(212)
        ax2.margins(0.05)           
        ax2.plot(self.t_array,self.p_array, color='cornflowerblue',linewidth=6)
        ax2.set_ylabel("P value",fontsize=50)
        ax2.set_xlim(left=self.t_array.min(), right=self.t_array.max())
        ax2.set_ylim(bottom=0, top=4)
        ax2.tick_params(labelsize=50)
        fig = plt.gcf()
        fig.set_size_inches(40,30)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/FIGURE_8_p_array_'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'.png')
        plt.close(fig)



    def plot_histogram_p_array(self):
        reject_high_pvalues=0
        if reject_high_pvalues==1:
            p_array_filtered=self.p_array[self.p_array<4]
            print('There were'+str(len(self.p_array)-len(p_array_filtered))+'days rejected from the array.')
            p_mean=np.mean(p_array_filtered)
            q75, q25 = np.percentile(p_array_filtered, [75 ,25])
            iqr = q75 - q25
            threshold=p_mean+1.5*iqr
            plt.hist(p_array_filtered, bins=39,range=(0,4),color='gray', edgecolor='black')  # arguments are passed to np.histogram
        else:
            p_mean=np.mean(self.p_array)
            q75, q25 = np.percentile(self.p_array, [75 ,25])
            iqr = q75 - q25
            threshold=p_mean+1.5*iqr
            plt.hist(self.p_array, bins=220,range=(0,22),color='gray', edgecolor='black')  # arguments are passed to np.histogram
        
        axes=plt.gca()
        axes.set_xlim(left=0,right=4)

        plt.axvline(p_mean)
        plt.axvline(threshold,linestyle='--')
#        plt.suptitle("Distribution of p-array values for station "+str(self.local.station_id))
#        plt.title("using "+str(self.remote.station_id)+" as a remote")
        plt.xlabel("P value")
        plt.ylabel("Counts")
        plt.title('Threshold='+str(threshold))
        fig=plt.gcf()
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/histogram_station_'+str(self.local.station_id)+'.png')
        plt.close(fig)
        self.threshold=threshold
#        self.threshold=1.5

        
    def compute_number_of_events(self):
        for idx, value in enumerate(ead.p_array):
            if value >= self.threshold:
                self.number_of_events=self.number_of_events+1
                
            
    def compute_p_count(self):
        p_count_1_day_stack=np.zeros(91)
        for idx,eq_date in enumerate(self.quakes.time):
            tdelta=datetime.datetime.combine(datetime.datetime.strptime(eq_date, '%Y-%m-%d %H:%M:%S.%f'), datetime.datetime.min.time())-self.local.start_date
            center_idx=tdelta.days
            print(center_idx)
            start_idx=center_idx-45
            end_idx=center_idx+46
            p_91=self.p_array[start_idx:end_idx]
            p_count=np.zeros(91)
            p_count[np.where(p_91>self.threshold)]=1
            p_count_1_day_stack=p_count_1_day_stack+p_count
        self.p_count_1_day_stack=p_count_1_day_stack

    def plot_anomalies_1_day(self,random_mean,random_std,es):
        colors=['bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','red','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','mediumspringgreen','bisque','bisque','bisque','bisque','bisque']
        plt.bar(np.arange(-45.5,45.5,1),self.p_count_1_day_stack,width=1,color=colors)
        plt.axvline(0,linestyle='--',color='black',linewidth=4)
        plt.plot(np.arange(-45.5,45.5,1),random_mean,linestyle='-',marker='o',color='blue',linewidth=4, label='Random_mean')
        plt.plot(np.arange(-45.5,45.5,1),random_mean+2*random_std,linestyle='-',marker='.',color='blue',linewidth=4,label='Random_mean+2$\sigma$')
#        plt.suptitle("log(Es)>"+str(es),fontsize=40)
        plt.title(str(self.number_of_quakes)+" EQ",fontsize=40)
        plt.xlabel('Days from EQ ',fontsize=40)
        plt.ylabel("Counts",fontsize=40)
        plt.xticks(fontsize=40)    
        plt.yticks(fontsize=40)
        fig = plt.gcf()
        fig.set_size_inches(40,20)
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'+str(self.local.station_id)+'_geomagnetic_anomalies_1_day_stack.png')
        plt.close(fig)
#        
    def save_p_array_to_dataframe(self):
        padf=pd.DataFrame()
        padf['p_array']=self.p_array
        padf['t_array']=self.t_array
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')        
        padf.to_csv(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'+str(self.local.station_id)+"_p_array.csv")
        
        
    def save_p_count_to_numpy_file(self):
        if not os.path.exists(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'):
            os.mkdir(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/')        
        np.save(str(root_folder)+str(output_folder)+'/'+str(self.local.station_id)+'_vs_'+str(self.remote.station_id)+'/'+str(self.local.station_id)+"_p_count_1_day", self.p_count_1_day_stack)


    def plot_molchan_diagram(self):
        print("Work in progress: Molchan diagram not implemented yet")        
        

class FiveDaysCount(object):

     def __init__(self):
         self.data = None
         self.number_of_quakes = 0
         self.station_id= None
         
     def populate(self,p_count_1_day_stack,local,number_of_quakes):
         p_count_5_days_stack=np.zeros(18)
         for i in np.arange(0,9):
             p_count_5_days_stack[i]=np.sum(p_count_1_day_stack[0+5*i:5+5*i])
         for i in np.arange(9,18):
             p_count_5_days_stack[i]=np.sum(p_count_1_day_stack[46+5*(i-9):51+5*(i-9)])
         self.data=p_count_5_days_stack
         self.number_of_quakes=number_of_quakes
         self.station_id=local.station_id

     def plot_anomalies(self,random_mean_5_days,random_std_5_days,es):
        plt.bar(np.arange(-42.5,47.5,5),self.data,width=4.6,color='bisque')
        plt.axvline(0,linestyle='--',color='black',linewidth=4)
        plt.plot(np.arange(-42.5,47.5,5),random_mean_5_days,linestyle='-',marker='o',color='blue',linewidth=4, label='Random_mean')
        plt.plot(np.arange(-42.5,47.5,5),random_mean_5_days+2*random_std_5_days,linestyle='-',marker='o',color='orange',linewidth=4,label='Random_mean+2$\sigma$')
#        plt.plot(np.arange(-45,45,5),random_mean_5_days,linestyle='-',marker='o',color='blue',linewidth=4, label='Random_mean')
#        plt.plot(np.arange(-45,45,5),random_mean_5_days+2*random_std_5_days,linestyle='-',marker='o',color='orange',linewidth=4,label='Random_mean+2$sigma')
#        plt.suptitle("log(Es)>"+str(es),fontsize=50)
        plt.title(str(self.number_of_quakes)+" EQ",fontsize=55)
        plt.xlabel('Days from EQ ',fontsize=55)
        plt.ylim(top=35)
        plt.ylabel("Counts",fontsize=55)
        plt.xticks(fontsize=55)    
        plt.yticks(fontsize=55)
        plt.legend(fontsize=55)
        fig = plt.gcf()
        fig.set_size_inches(40,25)
        plt.savefig(str(root_folder)+str(output_folder)+'/'+str(self.station_id)+'/'+str(self.station_id)+'REGEN_geomagnetic_anomalies_5_day_stack.png')
        plt.close(fig)

     def save_p_count_to_numpy_file(self):
        np.save(str(root_folder)+str(output_folder)+'/'+str(self.station_id)+'/'+str(self.station_id)+"_p_count_5_days", self.data)



#MAIN PROGRAM

if __name__=='__main__':
 
    create_output_folders_and_subfolders(root_folder,output_folder) ## Create folders and subfolders where to save the ouput figures. 
    back_up_the_configuration_file(root_folder,output_folder) ## Create a back up of myconfig.py into the output folder
    identify_region(min_radius)    	

    quakes=Quakes()
    dq, A=quakes.select_qualifying_earthquake_days(manually_select_quakes)

    pdb.set_trace()    

    
    
####UNCOMMENT TO PLOT DETAILED SIGNALS ON ANY GIVEN DAY   
    
#    qtid = QuietTimeIntervalDoublet()
#    qtid.populate('kak','kny',datetime.datetime(2002, 1, 1, 0, 0),'X','db5')
#    qtid.populate('kak','kny',datetime.datetime(2002, 1, 1, 0, 0),'Z','db5')
#    qtid.plot_detailed_signals_in_the_sixth_level()  
#    qtid.plot_psd()

    

    ea_local = EnergyArray()
    ea_local.populate('kak', start_date,end_date, 'Z',list_of_days_to_reject)
    ea_local.plot_energy_array()
    ea_remote = EnergyArray()
    ea_remote.populate('kny', start_date,end_date, 'Z',list_of_days_to_reject)
    ea_remote.plot_energy_array()
    
    
    ead=EnergyArrayDoublet()
    ead.populate(ea_local,ea_remote,dq)
    ead.compute_number_of_quakes()
    ead.produce_correlation_plot()
    ead.compute_t_array()
    ead.compute_p_array()
    ## Here you may reject anomalous p values using the reject_anomalous_p_values function

    
#    ead.plot_Z_and_p_arrays()
    ead.plot_energy_and_P_values_over_30_days_FIGURE_7()
    ead.plot_energy_and_P_values_over_10_years_FIGURE_8()
    
    print('HISTOGRAM')
    ead.plot_histogram_p_array()
    ead.compute_number_of_events()
    #ead.plot_molchan_diagram()
    
    
    ead.plot_p_values_with_quake_dates()
    ead.compute_p_count()
    p_count_1_day_stack_over_NITER,random_mean,random_std=compute_random_mean(niter,dq.number_of_quakes,ead.local.start_date,ead.local.end_date,ead.p_array,ead.threshold)
    
    ead.plot_anomalies_1_day(random_mean,random_std,np.log10(integrated_es))
    
    ead.save_p_array_to_dataframe()
    ead.save_p_count_to_numpy_file()    
    
    fdc=FiveDaysCount()
    fdc.populate(ead.p_count_1_day_stack,ead.local,dq.number_of_quakes)
    p_count_5_days_stack_over_NITER,random_mean_5_days,random_std_5_days=compute_5_days_stack_over_NITER(p_count_1_day_stack_over_NITER,niter)
    
    fdc.plot_anomalies(random_mean_5_days,random_std_5_days,np.log10(integrated_es))
    fdc.save_p_count_to_numpy_file()
    print(ead.number_of_events)