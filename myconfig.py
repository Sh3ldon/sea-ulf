import datetime

root_folder='E:/'
input_folder='usgs_japan_pre2010'
output_folder='MANUAL'

start_date=datetime.datetime(2001,1,1)
end_date=datetime.datetime(2011,1,1)



niter=10000
station_number=452
integrated_es=10**8
min_depth=0
max_depth=60
min_radius=0
max_radius=100
manually_select_quakes=0
catalog='JMA'

#date_list=['2001-02-24 12:00:00.0','2001-02-26 12:00:00.0','2001-04-17 12:00:00.0','2001-07-31 12:00:00.0','2001-09-04 12:00:00.0','2001-12-07 12:00:00.0','2002-06-19 12:00:00.0','2003-03-02 12:00:00.0','2003-04-07 12:00:00.0','2003-11-22 12:00:00.0','2004-08-20 12:00:00.0','2004-09-01 12:00:00.0','2004-10-16 12:00:00.0','2004-10-23 12:00:00.0','2004-10-24 12:00:00.0','2004-10-27 12:00:00.0','2004-11-03 12:00:00.0','2004-11-05 12:00:00.0','2004-11-08 12:00:00.0','2004-11-09 12:00:00.0','2005-04-03 12:00:00.0','2005-05-19 12:00:00.0','2005-08-07 12:00:00.0','2005-10-22 12:00:00.0','2006-02-03 12:00:00.0','2006-03-13 12:00:00.0','2006-04-20 12:00:00.0','2006-05-02 12:00:00.0','2006-09-06 12:00:00.0','2007-06-20 12:00:00.0','2007-07-16 12:00:00.0','2007-09-20 12:00:00.0','2007-11-26 12:00:00.0','2008-02-08 12:00:00.0','2008-03-24 12:00:00.0','2008-05-02 12:00:00.0','2008-05-07 12:00:00.0','2008-05-08 12:00:00.0','2008-10-25 12:00:00.0','2008-12-20 12:00:00.0','2008-12-21 12:00:00.0','2008-12-23 12:00:00.0','2009-01-31 12:00:00.0','2009-04-21 12:00:00.0','2009-06-06 12:00:00.0','2009-09-01 12:00:00.0','2009-12-17 12:00:00.0','2010-06-13 12:00:00.0','2010-09-29 12:00:00.0']
date_list=[]



list_of_days_to_reject=[datetime.datetime(2001, 3, 28, 0, 0),datetime.datetime(2001, 3, 29, 0, 0), datetime.datetime(2001, 3, 30, 0, 0), datetime.datetime(2001, 3, 31, 0, 0), datetime.datetime(2001, 4, 11, 0, 0), datetime.datetime(2002, 8, 3, 0, 0), datetime.datetime(2003, 1, 30, 0, 0), datetime.datetime(2003, 10, 24, 0, 0), datetime.datetime(2003, 10, 25, 0, 0), datetime.datetime(2003, 10, 29, 0, 0), datetime.datetime(2003, 10, 30, 0, 0), datetime.datetime(2003, 10, 31, 0, 0), datetime.datetime(2003, 11, 20, 0, 0),  datetime.datetime(2004, 11, 7, 0, 0),  datetime.datetime(2004, 11, 8, 0, 0), datetime.datetime(2004, 11, 9, 0, 0) , datetime.datetime(2004, 11, 10, 0, 0), datetime.datetime(2005, 1, 20, 0, 0), datetime.datetime(2005, 1, 21, 0, 0), datetime.datetime(2005, 10, 7, 0, 0), datetime.datetime(2010, 3, 25, 0, 0), datetime.datetime(2010, 4, 5, 0, 0)]
